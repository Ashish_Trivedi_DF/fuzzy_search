from django.shortcuts import render
from django.http import HttpResponse

import pandas as pd
import sys
import operator
import json


#creating home page where user can search for word

def index(request):
    return render(request,'home.html')

# result page where all search related query will show

def design(data):                                  
    f = open('/home/dfuser/Desktop/django/fuzzy/templates/result.html','w')
    data =json.dumps(data,indent=4)
    # data =json.loads(data)
    message = """<html>
    <head></head>
    <body>
    <center><h3>!!! Welcome In Fuzzy Search Engine  !!!</h3> 
            <form action="/find_word">
                 <input type="text" name="word" style="width: 40%;font-size: 20px;" ><br>
                <br>                
                <input  type="submit" value="Search" style="width: 20%; height: 6%; font-size: 20px;">
              </form></center>

    <p style="width: 40%;font-size: 20px;">{}</p></body>
    </html>""".format(data)

    f.write(message)
    f.close()
    return 

def Sort(tup): 
    return(sorted(tup, key = lambda x: x[1])) 


#Main logic for search mechanism

def fuzzy_module(request):
    word =request.GET['word'] 
    if word =="":
        design({"Status":"Please type any character"})
        return render(request,"result.html")

    #READING GIVEN FILE AND CONVERT INTO DATAFRAME AND MAKE IT ITERABLE
    data = pd.read_csv("/home/dfuser/Desktop/django/fuzzy/search_engine/word_search.tsv",sep='\t')

    data.rename(columns={'the':'word','23135851162':'frequency'},inplace=4)

    data['word'][333333]='the'
    data['frequency'][333333] ='23135851162'

    freq=[]
    val=[]

    dictonary = {'frequency':None,'value':None}        
    for frequency,value in zip(data['frequency'],data['word']):
        if word in str(value):
            freq.append(frequency)
            val.append(value)

    dictonary.update({'frequency':freq,'value':val})               
    freq_temp=[]
    val_temp=[]
    
     
    try:  
        #COUNTING 25 WORD IF WORD HAS THAT MUTCH SUGGATIONS
        if len(dictonary['value'])>25:
            for data in dictonary['value']:
                if data.startswith(word):
                    index = dictonary['value'].index(data)
                    freq_temp.append(dictonary['frequency'][index])
                    val_temp.append(dictonary['value'][index])
        else:
            #LOGIC FOR LESS THEN 25 WORD AND FILTERING OUT WITH FREQUENCY AND SHORT WORD
            tuples_list = []
            for freq, val in zip(dictonary['frequency'],dictonary['value']):
                tuples_list.append((freq,val))
            result=Sort(tuples_list)
            dictonary ={}
            final_list =[]
            
            for data in result:
                final_list.append(data[1])
            for i in range(len(final_list)):
                for j in range(i,len(final_list)):
                    if (len(final_list[j])<len(final_list[i])):
                        temp = final_list[j]
                        final_list[j] =final_list[i]
                        final_list[i] =temp 
            if len(final_list) == 0:
                final_list.append("search word is not available in file")             
            dictonary.update({"Search_Result":final_list})  

            design(dictonary)
            return render(request,"result.html")
        #LOGIC FOR MORE THEN 25 WORD AND FILTERING OUT WITH FREQUENCY AND SHORT WORD

        dictonary.update({'frequency':freq_temp,'value':val_temp})   
        test_dict={}
        for freq ,val in zip(dictonary['frequency'], dictonary['value']):
            test_dict.update({freq:val})
        sorted_d = sorted(test_dict.items(), key=operator.itemgetter(0),reverse=True)
        my_list=[]
        dictonary= {}
        print(sorted_d)
        for i in range(0,25):
            my_list.append(sorted_d[i])   
        tup_data =Sort(my_list)
        final_list=[]

        for data in tup_data:
            final_list.append(data[1])   
        for i in range(len(final_list)):
            for j in range(i,len(final_list)):
                if len(final_list[j])<len(final_list[i]) and (final_list[j].startswith(word)):
                    temp = final_list[j]
                    final_list[j] =final_list[i]
                    final_list[i] =temp    
        dictonary.update({"Search_Result":final_list})  
        
        design(dictonary)
        return render(request,"result.html")
    except:      
        return render(request,"result.html")
 